var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const mongoose = require('mongoose');

var jsonParser = bodyParser.json();

mongoose.connect('mongodb://localhost/FirstAppDB')
    .then(()=> console.log("Connected") )
    .catch(err=> console.log("Error", err));


const user = new mongoose.Schema({
    name: {type: String, required:true},
    password: {type: String}
});

const User = mongoose.model('users', user);

router.get('/', function(req, res, next) {
  res.send({ test: 'test' });
});



router.post('/login', jsonParser, function(req, res, next) {
  const body = req.body;
  const name = body.name;
  const password = body.password;

  console.log(password);

result = async function() {
  try {
    response = await User.findOne({name: name});
   if (response.password == password) {
    res.send ({log: 'done'});
   } else {
    res.send ({log: 'false'});
   };

  } catch (err) {
    res.send ({log: 'false'})
  }
}
result();
})

module.exports = router;
